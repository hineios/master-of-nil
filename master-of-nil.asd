;; Avoid namespace pollution (am I being to pedantic?)
(defpackage master-of-nil-system
       (:use :common-lisp :asdf))

(in-package :master-of-nil-system)

(asdf:defsystem "master-of-nil"
    :description "My personal blog written in Common Lisp"
    :version "0.1"
    :author "Fábio Almeida <fabio@masterofnil.com>"
    :license "MIT License"
    :depends-on ("hunchentoot" "swank")
    :components ((:file "packages")
		 (:file "master-of-nil" :depends-on ("packages"))))
