# Master of None

[![pipeline status](https://gitlab.com/hineios/master-of-nil/badges/master/pipeline.svg)](https://gitlab.com/hineios/master-of-nil/commits/master)
[![coverage report](https://gitlab.com/hineios/master-of-nil/badges/master/coverage.svg)](https://gitlab.com/hineios/master-of-nil/commits/master)

My personal blog built with Common Lisp


## Running locally

```bash
docker-compose up

```

This command runs the database container (non-persistent) and the web server container. The web server runs a HTTP server on port `80` and a Swank server on port `4005`.
