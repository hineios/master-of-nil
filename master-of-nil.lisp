(in-package :master-of-nil)

(defvar *http-server* nil)
(defvar *http-server-port* 5000)
(defvar *swank-server-port* 4005)

(defun main (&rest args)
  (declare (ignore args))

  ;; Determine the port where to start the HTTP server at runtime
  (when (uiop:getenv "PORT") (setf *http-server-port* (parse-integer (uiop:getenv "PORT"))))

  (setf *http-server* (make-instance 'hunchentoot:easy-acceptor
				     :port *http-server-port*
				     :document-root #P"/quicklisp/local-projects/master-of-nil/public/"))
  (hunchentoot:start *http-server*)
  (format t ";; Hunchentoot started on port: ~a.~%" *http-server-port*)

  (when (uiop:getenv "DEBUG")
    (setf swank::*loopback-interface* "0.0.0.0")
    (swank:create-server :port *swank-server-port*
			 :style swank:*communication-style*
			 :dont-close t))

  ;; and keep it going
  (handler-case (bt:join-thread (find-if (lambda (th)
					   (search "hunchentoot" (bt:thread-name th)))
					 (bt:all-threads)))
    ;; Catch a user's C-c
    (sb-sys:interactive-interrupt
	() (progn
	     (format *error-output* "Aborting.~&")
	     (hunchentoot:stop *http-server*)
	     (uiop:quit)))
    (error (c) (format t "Woops, an unknown error occured:~&~a~&" c))))
