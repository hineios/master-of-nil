FROM parentheticalenterprises/sbcl-quicklisp-base:1.5.6-2019-08-13
# https://github.com/fisxoj/lisp-images#base

RUN useradd -m lisp

# Place the project where quicklisp will find it
COPY . /quicklisp/local-projects/master-of-nil

RUN chown -R lisp:lisp /quicklisp
RUN chmod 755 /quicklisp

USER lisp

RUN sbcl --disable-debugger --eval "(load \"/quicklisp/setup.lisp\")" --eval "(ql:quickload :master-of-nil)" --eval "(sb-ext:save-lisp-and-die \"/home/lisp/masterofnil\" :toplevel 'master-of-nil:main :executable t)"


WORKDIR /home/lisp
CMD ["./masterofnil"]